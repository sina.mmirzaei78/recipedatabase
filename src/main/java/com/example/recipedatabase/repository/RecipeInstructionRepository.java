package com.example.recipedatabase.repository;

import com.example.recipedatabase.model.RecipeInstruction;
import com.sun.java.swing.plaf.windows.WindowsTextAreaUI;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RecipeInstructionRepository extends PagingAndSortingRepository<RecipeInstruction , Integer> {
//by default this Repo has its own CRUD methods!
}
