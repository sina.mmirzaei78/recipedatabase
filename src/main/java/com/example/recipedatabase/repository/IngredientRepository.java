package com.example.recipedatabase.repository;

import com.example.recipedatabase.model.Ingredient;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional
public interface IngredientRepository extends PagingAndSortingRepository<Ingredient , Integer> {
    Optional<Ingredient> findByIngredientName(String name);
    Optional<Ingredient> findByIngredientNameLike(String name);
}
