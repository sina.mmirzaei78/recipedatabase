package com.example.recipedatabase.repository;

import com.example.recipedatabase.model.RecipeIngredient;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RecipeIngredientRepository extends PagingAndSortingRepository<RecipeIngredient , Integer> {
//by default this Repo has its own CRUD methods!
}
