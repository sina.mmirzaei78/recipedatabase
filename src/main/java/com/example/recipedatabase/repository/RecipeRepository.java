package com.example.recipedatabase.repository;

import com.example.recipedatabase.model.Recipe;
import com.example.recipedatabase.model.RecipeCategory;
import jdk.internal.dynalink.linker.LinkerServices;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Transactional
public interface RecipeRepository extends PagingAndSortingRepository<Recipe , Integer> {
    List<Recipe> findAllByRecipeNameContains(String name);

    @Query(value = "select a.recipe_id , a.recipe_name from recipe a join recipe_ingredient b join ingredient c on a.recipe_id=b.recipe_id and b.ingredient_id=c.id where c.ingredient_name=?1", nativeQuery = true)
    List<Recipe> FindAllWithDescriptionQuery(String name);

    @Query(value = "select a.recipe_id , a.recipe_name from recipe a join recipe_recipe_category b join recipe_category c on a.recipe_id=b.recipe_id and b.recipe_category_id=c.category_id where c.category_id=?1", nativeQuery = true)
    List<Recipe> findAllRecipeWithSpecificRecipeCategory(int id);

    @Query(value = "select a.recipe_id , a.recipe_name from recipe a join recipe_recipe_category b join recipe_category c on a.recipe_id=b.recipe_id and b.recipe_category_id=c.category_id where c.category_name in ?1", nativeQuery = true)
    List<Recipe>findAllRecipeWithManyCategories(Set<String> categoryNames);
}
