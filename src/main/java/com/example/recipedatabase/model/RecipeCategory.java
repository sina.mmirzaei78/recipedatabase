package com.example.recipedatabase.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "recipeCategory")
public class RecipeCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int categoryId;
    private String categoryName;

    @OneToMany(mappedBy = "recipeCategory", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Recipe_RecipeCategory> recipe_recipeCategories;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<Recipe_RecipeCategory> getRecipe_recipeCategories() {
        return recipe_recipeCategories;
    }

    public void setRecipe_recipeCategories(List<Recipe_RecipeCategory> recipe_recipeCategories) {
        this.recipe_recipeCategories = recipe_recipeCategories;
    }
}
