package com.example.recipedatabase.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "RecipeIngredient")
public class RecipeIngredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int recipeIngredientId;
    private double amount;
    private Measurement measurement;

    @ManyToOne
    @JoinColumn(name = "ingredientId")
    @JsonIgnore
    private Ingredient ingredient;

    @ManyToOne
    @JoinColumn(name = "recipeId")
    @JsonIgnore
    private Recipe recipe;


    public int getRecipeIngredient() {
        return recipeIngredientId;
    }

    public void setRecipeIngredient(int recipeIngredient) {
        this.recipeIngredientId = recipeIngredient;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Measurement getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Measurement measurement) {
        this.measurement = measurement;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
}
