package com.example.recipedatabase.model;

public enum Measurement {
     KG("KG"), TBSP("TBSP"), TSP("TSP"), G("G"), HG("HG"), ML("ML"), CL("CL"), DL("DL");
     private final String name;

     Measurement(String name) {
          this.name = name;
     }


     @Override
     public String toString() {
          return "Measurement{" +
                  "name='" + name + '\'' +
                  '}';
     }
}
